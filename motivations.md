# Motivations érgonomiques

Je suis un ex-développeur informatique, quelqu’un qui a codé beaucoup et qui utilise aujourd’hui son clavier plusieurs heures par jours pour saisir de la prose et utiliser des logiciels professionnels. D’autre part, je suis aussi un joueur de jeux-vidéo quotidien.

Je saisissais à environ 60~70 mots par minutes en Azerty sans regarder mon clavier avec une très étrange habitude d’utiliser tous les doigts de ma main gauche mais un seul de ma main droite (si ce n’est pour atteindre la touche entrée). D’autre part j’utilise peu ma souris étant habitué aux raccourcis claviers et à naviguer dans les interface sans souris.

En 2014 il y a 5 ans j’ai commencé à manifester des douleurs différentes qui ont tous les symptomes du syndrome du canal carpien et des douleurs aigües au coude quand je le repose sur un bureau. Peu à peu ce problème s’est géneralisé sur les deux cotés.

Je me suis interessé de très prêt à l’érgonomie. D’autre part j’ai eu deux keypads gamer (des demis-claviers pour la main gauche) qui s’averaient très confortable comparé à mon clavier de tous les jours.

Après une fracture du poigné et avoir dû réapprendre la saisie j’en ai profité pour modifier mes habitudes…

## La posture

J’ai vu tout et son contraire sur la position à adopter devant un PC.
Je pense que suivre bêtement un dessin qu’a donné un médecin / ergonome est stupide. En effet, nous avons des tailles différentes, des bureaux de tailles différentes, des écrans de tailles différentes, des pieds ajustables ou non nous avons aussi accès à moins de ressouces dans certains milieux (Certaines sociétés ne sont pas prêtes à nous équiper convenablement, et parfois nous n’avons pas le budget pour ça…).

Pour ma part je siège sur un fauteuil Ikea Markus dont j’ai enlevé les acoudoirs. C’est un fauteuil confortable qui a une assise peut être un peu longue. Pour ma part je préfère largement avoir les avants bras qui sont à niveau du bureau sans avoir à trop écarté les coudes quand je saisie. Si mes coudes sont plus bas que mes mains alors j’obtiens vite de la douleur.

J’applique la règle d’avoir le haut de l’écran au niveau des yeux. Ainsi ma tête n’est pas trop inclinée vers le bas ni vers le haut. Je déconseille aux gens de rester toute la journée la tête baissée vers un petit écran de notebook.

J’utilise des pieds d’écrans réglables de manière à ce que mes écrans soient à un peu prêt 20~25 cm de haut par rapport au bureau. Mon notebook est aussi surelevé et je saisie sur un clavier secondaire qui lui est au niveau du bureau.

Je possède un repose-pied que j’utilise fréquemment.

Au final à mon avis la seule position parfaite est celle qui change. Le seul moyen que j’ai trouvé pour ne plus avoir de douleurs du tout est de bouger au fil de ma journée, faire varier la hauteur de mon siège, utiliser ou non mon repose pied. Travailler directement sur mon notebook sans clavier secondaire, rentrer chez moi et disposer d’une table à des hauteurs différentes etc.

Pour résoudre mes problèmes de « tendinite aux coudes » ?! (douleur aigues aux coudes) je me suis doté de repose poignet / main très éfficaces qui font presque toute la surface de mon bureau et ajoutent 3 cm d’épaisseur à celui-ci. le confort qu’ils procurent est génial.

## Changement de disposition

Une disposition de clavier c’est simplement l’emplacement des lettres des lettres sur votre clavier. Par exemple, il éxiste Azerty, Qwerty pour les Anglais, ou Qwertz pour les allemenads.

Ces dispositions sont presques identiques ce qui témoigne d’un ancetre commun. Elles ont toutes étées conçues sur des machines à écrire, et à l’époque, ces appareils avaient des contraintes techniques importantes.

Un petit retour en arrière s’impose…

Sur une machine à écrire, on se doit de pouvoir écrire vite en actionnant des sortes de marteaux contenant de l’encre sur la feuille. Il est proscrit d’utiliser deux marteau en même temps, sans cela, les marteaux se retrouvent bloqués. Plus les utilisateurs saisissaient vite, plus ils bloquaient les marteaux. C’est pourquoi les inventeurs des machines à écrire ont eu l’idée très simple d’éloigner les touches les plus utilisés de la langue Française et Anglaise. Du fait que les utilisateurs doivent déplacer les doigts d’une touche à une autre, il est plus rare d’arriver à un blocage des marteaux.

De ce besoin de solutionner ce problème élementaire a résulté de la création de deux dispositions encore utilisées aujourd’hui : Qwerty qui a été créée en 1873 pour les machines Remington et Azerty fin du XIXème.

Ce qui est fort c’est qu’en 2019 nous écrivons toujours avec une disposition dont l’objectif principale **est d’éloigner les touches les plus utilisées**.
Ah, et j’oubliais, c’est aussi l’objectif des rangées décalées de touches qui sont disposées en quinconce.

![mockup](https://media.giphy.com/media/3o7aCTfyhYawdOXcFW/giphy.gif)

Bref, ce clavier n’a rien pour lui à mes yeux.

Heuresement est donc arriver August Dvorak et quelques un de ces collègues qui ont inventé une disposition « Dvorak » pour l’Anglais qui a pour but de rassembler les touches les plus utilisées de la langue Anglaise sur la zone  de repos.
Le concept est simple, on pose les deux index sur les lettres qui comportent une petit repère en sur-épaisseur et les trois autres doigts sur les lettres de la rangée.

Ici un clavier Dvorak sur un Apple IIC illustrant la zone de repos en bleu ainsi que les petits repères présents normalement sur le F et le J en Azerty.

![zone-de-repos](https://i.imgur.com/mhaWciZ.png)

Dvorak est mort déçu en disant « I'm tired of trying to do something worthwhile for the human race. They simply don't want to change! » mais il n’a pas failli à sa tache car son héritié Français « Bépo » est né en 2005. C’est une disposition dédiée majoritairement au Français mais permettant de saisir dans toutes les langues européenes (jusqu’à l’Esperanto !).
Il favorise aussi l’utilisation de caractères spéciaux informatique et est donc agréable pour coder.

Voici quelques un de ces atouts (tirés du site officiel).

Le clavier permet de :

* obtenir une fréquence d’utilisation de la main gauche et droite presque équivalente.
* « sortir » le moins possible les doigts de la ligne de repos. En bépo, les deux tiers de la frappe se font sur la ligne de repos contre un peu plus de 20 % en azerty ;
* De répartir équitablement le nombre de frappes entre main gauche et main droite ;
* réduire la distance totale parcourue par les doigts de moitié par rapport à l’azerty ;
* réduire le nombre de digrammes à une main (¡ chiffres !) ;
* ne jamais avoir recours à la rangée du haut du clavier (chiffres et symboles) pour un texte en français ;
utiliser tous les types de claviers sans perte d’efficacité ;
simplifier la frappe des digrammes comprenant des caractères de la rangée inférieure du clavier, sous la main gauche (zone peu accessible sur les claviers décalés).

Il en résulte un grand confort d’utilisation, moins de fatigue et une sensation de facilité.

J’ai adopté cette disposition et mis 2~3 mois avant de saisir à 30~40 mots par minutes. Déjà je sentais un apport en confort malgré ma faible vitesse.
En un an j’ai réussi à saisir à 10 doigts en aveugle à 70 mots par minutes, en un an j’étais déjà plus rapide à l’aveugle qu’avec Azerty que j’avais utilisé pendant 15 ans.

Pour en finir sur la disposition

## Changement de clavier

Lors de mon adoption de Bépo j’ai aussi souhaité faire rupture avec l’usage de clavier traditionnel. Certaines choses élementaires me génaient :

* Je n’utilise pas capslock.
* Je n’aime pas aller chercher le clavier numérique et les flèches de navigations à droite. C’est un truc auquel on est habitué mais qui franchement n’a pas de sens. Sur tous les logiciels ou on doZQSD pour les jeux videos et JKLM pour les éditeurs de textes avancés comme VI.
* Le clavier numérique est très utile mais trop rarement. Je n’aime pas passer par dessus pour atteindre ma souris.
* J’utilisais beaucoup les tabulations / shift tabulation pour naviguer dans les champs de saisie comme j’ai appris à le faire sur terminal AS-400.
* Je trouve stupide les positions de la touche Entrée (surtout sur un clavier ISO) / Retour et supprimer nécessitant de plier son petit doigt et aussi « Début » / « Fin » qui sont beaucoup utilisé pour la rédaction de prose.

Au sujet de l’érgonomie il est bon d’observer la différence entre un clavier normé ISO et un clavier ANSI ou encore avec une « Big ass enter ». Ces trois normes de claviers peuvent vous aider à trouver plus aisément la touche Enter. Pour ma part, je préfère les claviers ANSI à ISO.

J’ai cherché quelques modèles et j’ai très vite séparé en plus ou moins deux grosse catégories les claviers ergonomiques.
1. Les claviers standards qui comportent quelques changements. Cela peut être par des touches incurvées, une inclinaison du clavier (À ne pas négliger !), quelques touches qui sont déplacées etc.
2. Les claviers qui font « rupture » complète avec la norme ISO / ANSI.

Clairement mon idée était de me trouver « le meilleur clavier » peu importe qu’il ai un look d’extra-terreste.

Je suis tombé sur le TypeMatrix qui est une réference très connue, l’Ergodox, le Kinesis contoured et le Truly Ergonomic.
Je ne vais pas détailler les différentes claviers ergonomiques qui éxistent car il y en a des centaines, mais les features les plus communes sont : 

* Une largeur moins importante, souvent sans pavé numérique ou avec pavé numérique intégré, cela pour éviter des mouvements trop importants. (CF :  « Tenkeyless » 80%, ou les 60%…).
* Une hauteur moins importante. Parfois les touches de fonction sont accessible sur la ligne des chiffres.
* Emplacements des touches Ortholinéaire / matriciel : Adieu les touches de machines à écrire préhistoriques.
* [Une inclinaison particulière](http://xahlee.info/kbd/keyboard_forearm_pronation.html) (en Anglais « Tenting »). En effet, au repos nos mains ne sont pas faites pour être à plat mais plutôt le long de notre corps comme les playmobils. Idéalement, un clavier vertical serait meilleur.
* [Des touches dédiées à des raccourcis utiles et non des lanceurs ou « touches média »](http://xahlee.info/kbd/keyboard_special_buttons.html)
* [Des claviers séparés en deux pièces](http://xahlee.info/kbd/diy_keyboards_index.html). Avoir la main gauche à gauche et la main droite à droite n’est il pas plus logique que d’avoir les deux mains au centre ?

J’ai choisi finalement le typematrix, relativement dans mon budget comparé à d’autres qui semblaient plus qualitatifs mais deux fois plus chers.
C’est fabriqué en chine, et le nombre de feature ainsi que la disposition est vraiment cool et pour un clavier d’origine je n’ai jamais trouvé mieux selon mes critères personnels.

## Changement de periphériques de pointage

Pour ce qui est des périphériques de pointages (comme la souris), il éxiste plusieurs solutions ergonomiques très interessantes.

* Les trackpad qui permettent d’éviter de bouger « trop » poignet comme sur une souris.
* Les [trackballs](http://xahlee.info/kbd/trackball_index.html) qui ont le même atout mais encore plus prononcé : Une fois la main dessus, le poignet ne bouge plus. Les mouvements plus longs sont parfois facilités. Il en existe des tas de sorte : La sphère peut varier en taille, elle peut être utilisable avec le pouce ou d’autres doigts. Le trackball peut être déplacable comme une souris, les boutons sur le pouce ou les index etc. Voilà quelques petites choses à savoir :
    * Beaucoup de modèles sont moisis en dessous de 50€. Vous risquez de detester un mauvais trackball.
    * Plus la sphère est lourde et grande, plus c’est agréable. Plus la sphère est petite, moins précis sont les mouvements. Plus la sphère est grande, plus le boitier est grand et peut avoir une très mauvaise inclinaison qu’il faudra compenser avec des support etc.
    * Il faut un moyen de faire du défilement (scroll), soit par une une molette de défilement, soit avec une sphère cliquable. Tous les modèles qui n’en ont pas sont à proscrire.

* Le trackpoint d’IBM permettant de ne pas décrocher les doigts du clavier pour réaliser les petits mouvements de pointeurs. Ce n’est pas une subtitution à la souris, mais c’est quelque chose que j’aime beaucoup.
* Ultranav d’IBM. Le système qui combine un trackpad et un trackpoint.
* Rollermouse. Certainement la Royce-Rolls des périphériques de pointage.
* Les souris verticales. je n’apprécie pas réèlement ce produit. C’est clairement une position plus naturelle que la souris normale. Mais d’une part j’ai du mal à avoir de la précision avec (car je ne peux pas la freiner avec ma paume) et surtout je n’aime pas avoir à lever ma main plus haut pour passer au dessus de la souris afin de m’en saisir. C’est peut-être une question d’habitudes.

J’ai choisi de me doter d’un trackball Kensigton « Expert mouse ». Attention avec ça. Il y a vraiment beaucoup de réferences qui sont insuportables. Xahlee.info est une bonne source d’info pour choisir le votre.
Je ne regrette pas du tout cet achat mais je dois avoué qu’il m’a demandé un temps d’adaptation important car la position n’était pas naturelle au début. J’ai aussi dû inverser son inclinaison d’origine.

