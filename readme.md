# **A complete how-to design your personal keyboard**

*French version is available too on this repository*

I dream about my dream keyboard too long time so I have learned how to build it myself.

This repository a mix of :

* Tutorial / step to step
* Build-log
* Many ressource links

# Introduction of my dreamed keyboard, the « Thinkmatrix »

![Thinkmatrix-mockup](./pictures/thinkmatrix-mockup.png)

## Design :

* 3D printed body
* 3D printed plate or laser-cutted plate of acrylic / aluminium
* Mechanical switchs matrix handwired OR PCB mounted (maybe a home-made PCB)

## Layout :

* Ortholinear keys layout like the TypeMatrix 2030
* Centered Enter / Backspace / Delete like the TypeMatrix 2030
* Direct access to cut copy and paste.

## Hardware :

* IBM Ultranav with Trackpoint, trackpad, and mouse button
* Logitech scroll-ring with infinity scroll and two horizontal scroll keys
* Plate or body designed for supporting all the Cherry MX compatible switch : Gatheron, Zealoth, Kailh standard and low profile switchs et cætera…
* USB and micro USB Hub.
* Integrated raspberry
