# **Un guide complet pour designer et fabriquer votre clavier**

*La version Anglais de ce guide est disponible aussi sur ce dépôt git*

J’ai trop longtemps révé de fabriquer mon propre clavier, donc j’ai souhaité apprendre à le réaliser moi-même.

Ce dépôt est un mix entre :

* Un tutoriel / Guide pas à pas.
* Un « Build-log » ou je détaillerai mes aventures.
* Un lieu ou je stocke énormément de liens et de ressources web sur les claviers.
* Un livre d’histoire.

# 0. Introduction sur mon projet, le « Thinkmatrix »

![Thinkmatrix-mockup](./pictures/thinkmatrix-mockup.png)

## Design :

* Chassis imprimé en 3D.
* Plaque de support des switchs imprimée en 3D ou découpée en acrylique / métal par un laser de découpe.
* Matrice d’interupteurs mécaniques soudés à la main ou monté sur un PCB (qui sera peut être fabriqué à la main).

## Disposition :

* Matricielle / Ortholinéaire tout comme le TypeMatrix 2030
* Touche entrée / retour / suppr. centrées comme le Typematrix 2030
* Accès direct à des raccourcis Copier / Coller.

## Objectifs matériel :

* Plaque ou chassis qui supportera les interupteurs compatibles Cherry MX par exemple les Gatheron, Zealoth ou Kailh low-profile et cætera…
* Présence d’un IBM Ultranav qui combine le Trackpoint, le trackpad et les boutons de souris
* Molette de défilement Logitech avec « infinite scroll » et défilement horizontale

Un jour peut-être…
* Un hub USB
* Un raspberry intégré au chassis :)

---

# 2. Choix de la méthode de fabrication

## PCB ou soudures à la main ?

Pour comprendre les solutions que nous avons à disposition il faut comprendre de maniège génerale ce que sera le schéma de cablage d’un clavier.

Peut importe la méthode que l’on choisiera, le shéma électronique restera le même. Il nous faudra créer une sorte de matrice avec des colonnes de cables et des lignes de cables qui se croisent au niveau des switchs. Pour finir on reliera les lignes et les colonnes au micro-controlleur.

![Exemple d’une matrice](https://www.baldengineer.com/wp-content/uploads/2017/12/9x9-Keyboard-Matrix.jpg)

Sur cet exemple nous constatons qu’il y a des switchs mais aussi un composant triangulaire qui représente une diode. Le principe des diodes est de laisser passer le courant seulement dans un sens. Elles sont donc « polarisées » (on ne peut pas les utiliser dans l’autre sens). Certaines éclaires, on les appelles les diodes electro-luminescentes (LED / DEL) mais ici on parle bien de diodes qui ne produisent pas de lumière.

Les industriels ont réussi à se passer de diodes en utilisant des astuces pour réduire les coûts. Mais en general c’est au détriment des performances (Voir : NKRO).

Il existe trois manières de créer le circuit électronique que je vous ordonne, en premier les méthodes simples, en dernier les méthodes complexes et « abouties ».

1. Un clavier soudé à la main. C’est ce qui sera le plus simple à la fois pour des prototypes et des claviers finaux. Les avantages sont nombreux mais le plus important c’est que cela nécessite peu de materiel et qu’on a pas besoin d’être chimiste !
2. Un circuit imprimé. Il y peut être réalisé à la main avec un peu de chimie, ou alors créé par informatique puis réalisé par des machines personnelles (CNC) ou par des professionnels.
3. Une membrane en plastique comme sur les claviers industriels. C’est peu cher à produire en masse, mais pour nous c’est complètement hors sujet.

Pour ma première version de ThinkMatrix j’utiliserai la soudure à la main.

## Matériaux

La seule pièce nécessaire d’un clavier est la plaque qui permet de supporter les switchs. Le reste n’est que cosmétique.

Pour la plaque qui mesurera environ 1,5~1,6mm, il y a plusieurs solutions possibles.

* Le bois
    * Dur de trouver un bois suffisament robuste qui supporte une faible épaisseur.
    *  On trouve du bois un peu partout.
    *  L’aspect est beau, une fois traité.
    *  Il aura peu de résonance lors de la saisie.
* Le métal.
    * Il s’agit de découper une plaque avec une machine CNC.
    * Solution très utilisée par les makers.
    * Il faut la faire découper par une société spécialisée ou disposer d’un laser à découpe très puissant.
    * En France il y a [John Steel](https://www.john-steel.com/fr/).
    * Robuste.
    * Bel aspect surtout avec le choix de métaux disponible : Acier korten dont l’aspect est rouillé, alu brossé, traitements miroirs…
    * Le désavantage : La résonance lors de la saisie.
* Les feuilles d’acrylique.
    * On utilise laser à découpe.
    * Il y a du choix de couleur, des possibilités de transparence.
    * La résonance est moindre par rapport au metal.
* Une plaque imprimée en 3D.
    * Le choix du plastique ou de la méthode d’impression sera important car la rigidité doit être importante. En somme, soit on imprime en PETG / ABS pour avoir une plaque rigide ce qui nécessite de maitriser ce procédé d’impression, soit on utilise un plastique souple que l’on rigidifie grâce à une structure contraignante à concevoir et contraignante pour le design.

Pour le chassis plusieurs options s’offrent à nous :

* Avec seulement la plaque et quelques entretoises (spacer en Anglais, des sortes de rondelles épaisses et cylindriques) on arrive à surélever la plaque suffisamment pour la rendre utilisable.
* On utilise une seconde plaque et des spacer pour former un clavier « sandwich ». C’est l’une des méthodes les plus simples quand on fabrique un clavier à l’aide de plaque d’acrylique ou de métal.
* Impression 3D.
* Bois.

Pour ma part, je vais tout concevoir en impression 3D. J’aime l’idée d’être autonome et je ne sais pas travailler le bois pour concevoir un châssis en bois.

![exemple](https://d3jqoivu6qpygv.cloudfront.net/img_bucket/ergodox/ergodox_assembly_layer_03.png)
![exemple](http://www.gonskeyboardworks.com/163-thickbox_default/universal-frameplate-for-60.jpg)
![exemple](http://reho.st/self/6fd3adb932298d17f1d639e0bfc40c900b5c0262.jpg)

[Plus d’infos sur les plaques](https://deskthority.net/viewtopic.php?f=7&t=12755&start=)

# 3. Choix du matériel

Pour le clavier on vas avoir besoin de matériaux de base, de composants élecroniques, d’interupteurs et d’outils qui vont nous permettre d’atteindre nos objectifs.

## Les outils

* Un multimètre premier prix avec fusible de sécurité et option « test » qui permet de vérifier rapidement si une connexion est faite.
* Un fer à souder premier prix avec température réglable et une « panne » fine.
* Une pince de précision un peu comme ça, ou avec un coude (c’est pareil) pour tordre les composants et faire des manipulation avec précision.

<img src="https://www.la3d.ch/wp-content/uploads/2017/11/db7a0ca8-1501-4fdf-838d-c1135be7c70f1.jpg" height="200px"/>

* Un jeu de petite pinces. Coupante, plate, à bec long. Vous n’aurez pas besoin de toutes mais c’est pratique…
* Une pince à dénuder.

| <img src="https://www.shopix.fr/19823-thickbox/pince-a-denuder.jpg" height="150px"/> | <img src="https://i2.cdscdn.com/pdt2/4/2/0/1/700x700/kst4042146124420/rw/pince-a-denuder-automatique.jpg" height="150px"/>      | <img src="https://www.leroymerlin.fr/multimedia/a84630682/produits/pince-a-denuder-automatique-dexter-160-mm.jpg" height="150px"/>      |
|---    |---    |---    |
|À chaque changement de diamètre donc c’est insupportable.|Demande de mettre le cable dans le bon trou mais ne doit pas être réglé à chaque changement de diamètre.| Réglage de diamètre automatique, on met le fil à l’arrache et on serre. + ermet de sertir les cosses et permet de dénuder en plein milieu d’un cable : Je vous conseille donc ce genre de pince.|


## Les consommables

* De l’étain. Ne faite pas comme moi, achetez de l’étain qui est fin ! Les échantillons avec les fer à souder sont bien trop épais et j’ai flingué un Arduino à cause de ça.
* Des câbles électriques. Je vous conseille les petits diamètres : 0,25mm et 0,75 si vous voulez de la rigidité sur certaines sections de votre montage. Vous pouvez aussi détruire un câble RJ45 pour obtenir un ensemble de fils colorés. Vous pouvez aussi acheter des nappes de fils pour certaines sections plus propres.
* De la gaîne thermorétractable et un briquet pour isoler de temps en temps.
* De l’insulation tape (scotch isolant) peut être pratique.
* Autant de diodes « 1 N4148 » que de switchs (nous l’utilité de ces diodes plus tard)


## Le micro-controlleur

Le micro-controlleur sera branché à l’ordinateur par USB contiendra le firmware du clavier. Il sera reconnu par l’OS et enverra les signaux des touches que l’on pressera.
Les features qui nous intéresse pour un clavier c’est d’une part le support de ce contrôleur par la communauté open-source et les firmware open-source (Comme QMK ou son ancêtre TMK) ainsi que le nombre de « pin » permettant d’avoir un nombre de lignes et de colonnes qui nous convienne sur le clavier.
Je ne m’intéresserai que au firmware open-source QMK car il est le plus aboutis.

Ces contrôleurs sont parfois Arduino compatibles mais globalement ils sont tous pareils :
* Des port + / - et des contacts qui permettent d’envoyer ou recevoir des informations.
* Un processeur AVR 8 / 16 bits ou ARM qui permet le traitement des informations via du code.

Il en existe pleins et j’en retiens la sélection suivante :

| Marque               | Réference  | Architecture   | Processeur     | Support de la communauté | Conseillé pour les débutants | Commentaire |
| --- | --- | --- | --- | --- | --- | --- |
| PJRC                 | Teensy 2.0 | AVR            | ATMEGA32U4     | **★★★★★** | Oui         | Très réputé, solide, très reconnu.                                                                              |
| PJRC                 | Teensy 2++ | AVR            | AT90USB1286    | ★★★★★ | Oui         | Idem que le 2 mais avec plus de pin. Permet des « grands claviers ».                                            |
| PJRC                 | Teensy 3.2 | ARM Cortex M4  | MK20DX256VLH7  | ★★      | Non         | Toute la suite ARM à installer. Peu accessible mais pourtant supporté par QMK.                                  |
| PJRC                 | Teensy 3.6 | ARM Cortex M4F | MK66FX1M0VMD18 | ★★      | Non         | Idem que le 3.2                                                                                                 |
| Sparkfun (ou clones) | Pro Micro  | AVR            | ATmega32U4     | **★★★★★** | Oui         | Le plus connu de tous les controlleurs. Très copié par les chinois, très peu couteux mais nombre de pin limité. |
| Arduino  (ou clones) | Nano V3.0  | AVR            | ATmega328P     | ★       | Non         | Très peu supporté par QMK et peu utilisé par les makers.                                                        |
| QMK                  | Proton C   | ARM Cortex M4  | STM32F303CCT6  | ★★★★  | Pas encore… | Controlleur conçu par QMK donc il a beaucoup d’avenir. Néanmoins, je ne l’ai pas encore vu en action.           |

En conclusion, je vous conseillerai un pro-micro pour des petits clavier, sinon un teensy 2 pour un clavier normal, et un 2++ si vous souhaitez avoir des fonctionnalités très nombreuses et un clavier de grande taille.

## Les switchs

Les switchs sont les interupteurs des touches de notre clavier. Il en existe des tonnes et il est important de comprendre qu’il y a deux grandes familles.

* Les switchs Cherry MX compatibles. Cherry MX.
* Les switchs ALPS compatibles.

Ce qui compte est de prendre des switchs qui ont une emprunte compatibles à ces deux switchs qui sont les plus communs. Avec ces deux empruntes on s’assure d’avoir beaucoup de choix dans les marques, les modèles, et aussi les keycaps que l’on pourra choisir.

Il existe trois types de switchs :

 * Les tactiles qui ont un petit « bump » qui nous aide à comprendre qu’on a trouvé le point d’activation de l’interrupteur.
 * Les clicky qui en plus d’être tactile font un bruit caractéristiques (qui peut rendre fou vos collègues).
 * Les linéaires qui n’ont ni « bump » ni bruit. Quand on les presse, on atteint le point d’activation à un certain niveau mais on ne peut pas le savoir sans avoir une certaine habitude de son clavier.

![switchs type](https://media.steelseriescdn.com/blog/posts/a-guide-on-gaming-keyboard-switches/63d6e74f9a8a4dbaaae6dcaed0b76479.gif)

Parmi tous les fabriquant il existe beaucoup de variation de ces types de switchs. En géneral la couleur du switch indique son type, mais chaque fabriquant possède sa codification.

De plus d’autres paramètres sont à prendre en compte tel que :

* La masse d’activation → C’est à dire la force qu’il faut mettre pour activer l’interrupteur.
* La distance de parcours.
* La hauteur du switch. Certains sont plus « bas » (les low-profiles) pour réaliser des claviers plus fin.

[Liste complète des 500~ switchs qui existent](https://deskthority.net/w/index.php?title=Category:List_of_all_keyboard_switches&pageuntil=Fujitsu+Libertouch#mw-pages)

Je ne vous dirait pas quel switch choisir car chacun à ses préférences là-dessus, que ce soit la marque le type, le prix… par contre, je peux vous dire que j’apprend avec des cherry MX blue (clicky) sur un vieux clavier Razer et j’ai aussi acheté des switchs low-profile Kailh Choc Brown (tactile) et Red (linéaires).

## Les keycaps

Après avoir choisi vos interupteurs, il faudra trouver des cabochons de touches (des keycaps) qui sont compatibles. Il existe des keycaps Alps compatibles, Cherry MX compatible et aussi des keycaps compatibles avec les switchs plus rares.

Il existe encore plus de keycaps que de switchs, les différences sont les suivantes :
* Le type de profile, c’est à dire la forme des touches. C’est vraiment le nerf de la guerre.
![mockup](https://i.imgur.com/kFxqOm8g.jpg)
* Le matériaux utilisé qui change la résonance, le touché, la rigidité, sa manière de vieillir.
* Le niveau de transparence, seulement sur les libellés des touches ou sur toute la touche.
* La présence ou non de labels.
* La méthode d’impression ou de gravure des labels.
* La couleur des touches et des labels. Certains sets ont des thèmes (Star wars etc…).
*


Choisir des keycaps, couleurs, matériaux, marquage des touches, profils des keycaps (DSA, XDA, SA etc)…
Choisir le contrôleur : Arduino Léonardo, Teensy, Pro-Micro. Architecture différentes du chip : AVR 8bit / ARM.

# 4. Designer le clavier

Conception du chassis en 3D.
Création de la plaque des switchs via des outils (Keyboard layout designer + création automatique du fichier dxf de la plaque).

## Key sizes summary

*This keycaps maps doesn’t count additional mouse button for additionnal hardware like trackpoint / trakball.*

![Keycaps map](./pictures/keycaps/keycaps.jpeg)

|Numbers|Size|Color on the picture|Information|
|---| --|---|---|
| 13 | 1 x 0.75 | ![sample](./pictures/keycaps/1×0,75.jpeg) | Function keys wich can be replaced by 13 1×1 keys if you want full size Fn keys.  |
| 61 | 1 x 1    | ![sample](./pictures/keycaps/1×1.jpeg) | Alphanumericals keys |
|  4 | 1.5 x 1  | ![sample](./pictures/keycaps/1.5×1.jpeg) | Fn, Compose/Super, Alt, Alt-Gr,|
|  1 | 1 x 1.5  | ![sample](./pictures/keycaps/1×1.5.jpeg) | Backspace |
|  2 | 1 x 2    | ![sample](./pictures/keycaps/1×2.jpeg) | Right Ctrl, Left Shift |
|  2 | 2 x 1    | ![sample](./pictures/keycaps/2×1.jpeg) | Space |
|  2 | 1 x 1.25 | ![sample](./pictures/keycaps/1×1,25.jpeg) | Enter, Delete |

# 5. Fabrication
Imprimer le chassis et la plaque.
Multiples itérations.

# 5. Soudure du chassis.
Disposition des switchs sur la plaque
Pourquoi et comment souder les diodes sur la matrice du clavier.
Soudure des colonnes / des lignes.
Racordement de la matrice soudée au controlleur.

# 6. Développement du firmware

Introduction sur QMK (firmware open source) et ses features.

## Features

* Fast pressing two time Compose / Windows key toggle the key activation and provide a feedback led on the compose key

Installation de l’environnement de dev (Clone du projet avec git, utilisation de Linux Embedded sur Windows, utilisation du bootloader sur la carte Arduino).
Fork d’un clavier existant et modifs necessaire à notre projet.
Compilation du firmware.
